let isProd = process.env.NODE_ENV == 'production';

$(document).ready(function () {

    /*  new begin */
    function togglemenu() {
        $('.header-menu').toggleClass('active');
    }
    $(".nav-toggler").on('click', () => {
        togglemenu();
    });
    $(".nav-header a").on('click', () => {
        togglemenu();
    });
    /*  new end */


    $('.play-icon').map((i,el) => {
        console.log(el);
        drawPlayIcon(el);
    });

    $('.video-play-icon').map((i,el) => {
        console.log(el);
        drawPlayIconVideo(el);
    });

    $(".nav a").on("click", function () {
        $(this).parent().find("a").removeClass('active');
        $(this).addClass('active');
    });

    $("a[href*=\\#]").on("click", function(e){
        var anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $(anchor.attr('href')).offset().top - $('header').height()
        }, 777);
        e.preventDefault();
        return false;
    });

    if($('.video-frame').length > 0){
        $('.video-frame').magnificPopup({
            disableOn: 700,
            type: 'iframe',
            removalDelay: 160,
            preloader: false,
            closeOnBgClick: false,
            fixedContentPos: true
        });
    }

    $(".gallery").lightGallery();

    AOS.init();
    ymaps.ready(function(){
        let map = new ymaps.Map("contacts-map", {
            center: [55.76, 37.64],
            zoom: 15,
            controls: [
                'zoomControl'
            ]
        });
        let myGeocoder = ymaps.geocode(`г. Красноярск, ул. Северное шоссе 5 Г`);
        let myGeocoder2 = ymaps.geocode(`г. Новокузнецк, Проспект Курако д.51/1`);
        map.behaviors.disable('scrollZoom');
        Promise.all([myGeocoder2, myGeocoder]).then(values => {
            let center = [];
            values.map(res => {
                let placemark = new ymaps.Placemark(res.geoObjects.get(0).geometry.getCoordinates(), {
                    balloonContent : res.geoObjects.get(0).properties._data.text
                }, {
                    iconLayout: 'default#image',
                    iconImageHref: '../img/map-marker.png',
                    iconImageSize: [65, 65],
                    iconImageOffset: [-32, -60],
                    iconColor: '#ff0000',
                });
                map.geoObjects.add(placemark);
                center.push(res.geoObjects.get(0).geometry.getCoordinates());
            });
            console.log(center);
            map.setBounds(center);
        });
    });

    // $(".gallery").each(function () {
    //     $(this).magnificPopup({
    //         delegate: 'a',
    //         type: 'image',
    //         gallery: {
    //             enabled: true
    //         }
    //     })
    // })


    //
    // $(window).on('scroll', function () {
    //     let headerHeight = $('header').height();
    //     if(window.pageYOffset >= headerHeight){
    //         $('header').addClass('fluid');
    //     }else{
    //         $('header').removeClass('fluid');
    //     }
    // });

});

function drawPlayIcon(element){
    let size = 42;
    let draw = SVG(element).viewbox(0,0,size,size);
    let rect = draw.circle((size - 2)).attr({cx: size / 2, cy: size / 2, stroke: '#fff', fill: "transparent" }).stroke({width: 2}).addClass('circle');
    let polygon = draw.polygon(`${size / 2},${size / 2}`).fill('#fff').stroke({ width: 0 }).addClass('play');
    polygon.animate(1000).plot(`${23/70*size},${20/70*size} ${23/70*size},${53/70*size} ${54/70*size},${36/70*size}`);
}

function drawPlayIconVideo(element){
    let size = 42;
    let draw = SVG(element).viewbox(0,0,size,size);
    //let rect = draw.circle((size - 2)).attr({cx: size / 2, cy: size / 2, stroke: '#fff', fill: "transparent" }).stroke({width: 2}).addClass('circle');
    let polygon = draw.polygon(`${size / 2},${size / 2}`).fill('#fff').stroke({ width: 0 }).addClass('play');
    polygon.animate(1000).plot(`${23/70*size},${20/70*size} ${23/70*size},${53/70*size} ${54/70*size},${36/70*size}`);
}